package com.example.myapplication

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_custom_toast.*
import kotlinx.android.synthetic.main.alert_dialog_payroll.view.*
import org.json.JSONArray
import org.json.JSONException
import java.net.HttpURLConnection
import java.net.URL
import java.text.NumberFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class Payroll : AppCompatActivity(){


    lateinit var spinnerGajiBulan: Spinner
    lateinit var spinnerTahunGaji: Spinner
    lateinit var btnSimpanPayroll: Button

    var year = ArrayList<String>()
    val currentDateTime = LocalDateTime.now()

    lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payroll)
        supportActionBar?.hide()
        session = Session(applicationContext)


        spinnerGajiBulan = findViewById(R.id.spinnerGajiBulan)
        spinnerTahunGaji = findViewById(R.id.spinnerTahunGaji)
        btnSimpanPayroll = findViewById(R.id.btnSimpanPayroll)

        btnSimpanPayroll.setOnClickListener {
            //custom toast
            val customLayout = layoutInflater.inflate(R.layout.activity_custom_toast_payroll, constraintlayout)


            //Show Dialog
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.alert_dialog_payroll, null)

            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)

            mDialogView.tvDetailGajiBulan.setText(spinnerGajiBulan.getSelectedItem().toString()+" "+spinnerTahunGaji.getSelectedItem().toString())
            val mAlertDialog = mBuilder.show()

            mDialogView.btnInsertDetailPayroll.setOnClickListener {
                val toast = Toast(this)
                toast.duration = Toast.LENGTH_SHORT
                toast.setGravity(Gravity.TOP, 0, 0)
                toast.view = customLayout
                toast.show()
                insertDataPayroll().execute()
                mAlertDialog.dismiss()
            }
            mDialogView.btnCancelDInsert.setOnClickListener {
                mAlertDialog.dismiss()
            }


        }

        //spinner Tahun
        var tmpyear = currentDateTime.format(DateTimeFormatter.ofPattern("yyyy"))
        var yearNow = tmpyear.toInt()
        var start = yearNow-5
        for (i in start..yearNow ){
            year.add(i.toString())
        }
        var tahun = ArrayAdapter(this, android.R.layout.simple_spinner_item, year)
        tahun.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerTahunGaji.setAdapter(tahun)

        //Spinner Bulan
        var month = arrayOf(
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        )
        var bulan = ArrayAdapter(this, android.R.layout.simple_spinner_item, month)
        bulan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerGajiBulan.setAdapter(bulan)

    }



    inner class insertDataPayroll: AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {
            val requestQueue = Volley.newRequestQueue(this@Payroll)
            val stringRequest: StringRequest = object : StringRequest(
                Method.POST,
                getString(R.string.url).toString() + "User_HRD/insert_data_payroll",
                Response.Listener { response ->
                    if (response.equals("[]", ignoreCase = true)) {
                    } else {
                        // Process response
                        try {
                            val jsonArray = JSONArray(response)
                            val jsonObject = jsonArray.getJSONObject(0)

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("VOLLEY", error.toString())
                })
            {
                override fun getParams(): Map<String, String>? {
                    val params: MutableMap<String, String> = HashMap()
                    params["idHRD"] = session.getUserDetails().get("userID").toString()
                    params["bulanTahun"] = spinnerGajiBulan.getSelectedItem().toString()+" "+spinnerTahunGaji.getSelectedItem().toString()
                    return params
                }
            }
            requestQueue.add(stringRequest)
            return ""
        }
    }

}

