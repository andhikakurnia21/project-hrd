package com.example.myapplication

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_custom_toast.*
import org.json.JSONArray
import org.json.JSONException
import java.net.HttpURLConnection
import java.net.URL
import java.util.*


class Profile : AppCompatActivity() {
    lateinit var session: Session
    lateinit var edUsername: EditText
    lateinit var edEmail: EditText
    lateinit var edTglLahir: EditText
    lateinit var edJenisKelamin: EditText
    lateinit var tvLogout: TextView
    lateinit var btnUpdateProfile: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        supportActionBar?.hide()
        session = Session(applicationContext)
        val url = "http://rs-webservice.xyz/index.php/User_HRD/user/"+session.getUserDetails().get("userID")
        val customLayout = layoutInflater.inflate(R.layout.activity_custom_toast, constraintlayout)
        AsyncTaskHandleJson().execute(url)

        //text
        edUsername = findViewById(R.id.edUsername)
        edEmail = findViewById(R.id.edEmail)
        edJenisKelamin = findViewById(R.id.edJenisKelamin)
        edTglLahir = findViewById(R.id.edTglLahir)
        tvLogout = findViewById(R.id.tvLogout)
        //button
        btnUpdateProfile = findViewById(R.id.btnUpdateProfile)

        btnUpdateProfile.setOnClickListener {
            UpdateBio().execute()
            val toast = Toast(this)
            toast.duration = Toast.LENGTH_SHORT
            toast.setGravity(Gravity.TOP, 0, 0)
            toast.view = customLayout
            toast.show()
        }

        tvLogout.setOnClickListener {
            session.logoutUser()

            val lgout = Intent(this@Profile, MainActivity::class.java)
            startActivity(lgout)
            finishAffinity()
        }

        if(session.getUserDetails().get("userPosition").equals("HRD")){
            tvLogout.isClickable=true
            tvLogout.visibility= View.VISIBLE
        }else{
            tvLogout.isClickable=false
            tvLogout.visibility= View.INVISIBLE
        }

    }

    inner class AsyncTaskHandleJson : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {

            var text:String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                text = connection.inputStream.use{it.reader().use{ reader->reader.readText()}}
            }finally {
                connection.disconnect();
            }
            return text
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handleJson(result)
        }
    }
    private fun handleJson(jsonString: String?){
        val jsonArray=  JSONArray(jsonString)
        var x = 0
        while(x<jsonArray.length()){
            val jsonObject = jsonArray.getJSONObject(x)
            edUsername.setText(jsonObject.getString("nama"))
            edEmail.setText(jsonObject.getString("email"))
            edTglLahir.setText(jsonObject.getString("tgl_lahir"))
            edJenisKelamin.setText(jsonObject.getString("jenis_kelamin"))
            x++
        }
    }

    inner class UpdateBio : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {
            val requestQueue = Volley.newRequestQueue(this@Profile)
            val stringRequest: StringRequest = object : StringRequest(
                Method.POST,
                getString(R.string.url).toString() + "User_HRD/update_user",
                Response.Listener { response ->
                    if (response.equals("[]", ignoreCase = true)) {
                    } else {
                        // Process response
                        try {
                            val jsonArray = JSONArray(response)
                            val jsonObject = jsonArray.getJSONObject(0)

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("VOLLEY", error.toString())
                })
            {
                override fun getParams(): Map<String, String>? {
                    val params: MutableMap<String, String> = HashMap()
                    params["id"] = session.getUserDetails().get("userID").toString()
                    params["username"] = edUsername.getText().toString()
                    params["email"] = edEmail.getText().toString()
                    return params
                }
            }
            requestQueue.add(stringRequest)
            return ""
        }
    }
}
