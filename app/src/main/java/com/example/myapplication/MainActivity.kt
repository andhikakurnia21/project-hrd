package com.example.myapplication


import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONException
import java.util.*


class MainActivity : AppCompatActivity() {
    lateinit var edEmail: EditText
    lateinit var edPassword: EditText
    lateinit var btnLogin: CardView
    val url = "http://rs-webservice.xyz/index.php/User_HRD/login_user"
    lateinit var session: Session

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        edEmail = findViewById(R.id.edEmail)
        edPassword = findViewById(R.id.edPassword)
        btnLogin = findViewById(R.id.btnLogin)
        //Session
        session = Session(applicationContext)

        if(session.isLoggedIn())
        {
            if(session.getUserDetails().get("userPosition").equals("Karyawan")){
                var i:Intent= Intent(applicationContext, Dashbord_Karyawan::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(i)
                finish()


            }else if(session.getUserDetails().get("userPosition").equals("HRD")){
                var i:Intent= Intent(applicationContext, Dashboard_HRD::class.java)
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(i)
                finish()

            }
        }

        btnLogin.setOnClickListener {
            if (edEmail.getText().toString() != "" && edPassword.getText().toString() != "") {
                AsyncTaskHandleJson().execute();
            } else {
                Toast.makeText(
                    this@MainActivity,
                    "Email dan Password Tidak Boleh Kosong",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
    inner class AsyncTaskHandleJson : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {
            val requestQueue = Volley.newRequestQueue(this@MainActivity)
            val stringRequest: StringRequest = object : StringRequest(
                Method.POST,
                getString(R.string.url).toString() + "User_HRD/login_user",
                Response.Listener { response ->
                    if (response.equals("[]", ignoreCase = true)) {
                        Toast.makeText(
                            this@MainActivity,
                            "Cek Kembali Email dan Password Anda",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        // Process response
                        try {
                            val jsonArray = JSONArray(response)
                            val jsonObject = jsonArray.getJSONObject(0)

                            // Get important data
                            val userId = jsonObject.getString("id")
                            val userName = jsonObject.getString("nama")
                            val userPosition = jsonObject.getString("position")
                            val userEmail = jsonObject.getString("email")

                            //create session
                            session.createLoginSession(userId, userName, userPosition, userEmail);

                            // Staring HomeActivity
                            if (session.getUserDetails().get("userPosition") != null) {
                                if (session.getUserDetails().get("userPosition")
                                        .equals("Karyawan")
                                ) {
                                    val Dashbord_Karyawan = Intent(
                                        applicationContext,
                                        Dashbord_Karyawan::class.java
                                    )
                                    startActivity(Dashbord_Karyawan)
                                    finish()
                                } else if (session.getUserDetails().get("userPosition")
                                        .equals("HRD")
                                ) {
                                    val Dashboard_HRD = Intent(
                                        applicationContext,
                                        Dashboard_HRD::class.java
                                    )
                                    startActivity(Dashboard_HRD)
                                    finish()
                                }
                            }

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("VOLLEY", error.toString())
                })
                 {
                    override fun getParams(): Map<String, String>? {
                        val params: MutableMap<String, String> = HashMap()
                        params["email"] = edEmail.getText().toString()
                        params["password"] = edPassword.getText().toString()
                        return params
                    }
                }
            requestQueue.add(stringRequest)
            return ""
        }
    }
}


