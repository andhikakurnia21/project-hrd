package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.custom_grid_view.view.*
import org.eazegraph.lib.charts.PieChart
import org.eazegraph.lib.models.PieModel
import org.json.JSONArray
import java.net.HttpURLConnection
import java.net.URL
import java.util.*


class Dashboard_HRD : AppCompatActivity() {

    lateinit var tvUsernameHRd: TextView
    lateinit var tvPosisi: TextView
    lateinit var session: Session
    lateinit var imgProfile: ImageView
    lateinit var gridView: GridView

    lateinit var tvR: TextView
    lateinit var tvPython: TextView
    lateinit var pieChart: PieChart
    var adapter: CustomAdapter? = null

    @RequiresApi(Build.VERSION_CODES.O)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard__h_r_d)
        setTitle("Dasboard")

        //Chart
        val urlKaryawan = "http://rs-webservice.xyz/index.php/User_HRD/jumlah_karyawan/"
        val urlHRD = "http://rs-webservice.xyz/index.php/User_HRD/jumlah_HRD/"
        AsyncTaskHandleJsonChartKaryawan().execute(urlKaryawan)
        AsyncTaskHandleJsonHRD().execute(urlHRD)

        tvR = findViewById(R.id.tvR);
        tvPython = findViewById(R.id.tvPython);
        pieChart = findViewById(R.id.piechart);
        pieChart.startAnimation();

        //Session
        session = Session(applicationContext)
        session.getUserDetails().get("userName")
        session.checkLogin()

        var user:HashMap<String, String> = session.getUserDetails()
        var name:String  = user.get(Session.KEY_NAME)!!
        var position:String = user.get(Session.KEY_POSITION)!!

        //Initialize
        tvUsernameHRd = findViewById(R.id.tvUsernameHRd)
        tvPosisi = findViewById(R.id.tvPosisi)
        imgProfile = findViewById(R.id.imgProfile)
        gridView = findViewById(R.id.gridMenu)


        //Async
        val url = "http://rs-webservice.xyz/index.php/User_HRD/user/"+session.getUserDetails().get("userID")
        AsyncTaskHandleJson().execute(url)

        //Profile
        imgProfile.setOnClickListener {
            val Profile = Intent(applicationContext, Profile::class.java)
            startActivity(Profile)
            //finish()
        }

        // load Menu GridView
        val menulist = ArrayList<Int>()
        menulist.add(R.drawable.approval)
        menulist.add(R.drawable.payroll)
        menulist.add(R.drawable.report)
        menulist.add(R.drawable.calendar2)
        menulist.add(R.drawable.status)

        val judul = arrayOf("Approval", "Payroll", "Report Payroll","Ajukan Cuti","Status Approval")

        val customAdapter = CustomAdapter(this@Dashboard_HRD, menulist, judul)

        gridView.adapter = customAdapter


        gridView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            if (position == 0) {
                val Approval = Intent(this@Dashboard_HRD, Approval::class.java)
                startActivity(Approval)

            }else if (position == 1){
                val Payroll = Intent(this@Dashboard_HRD, Payroll::class.java)
                startActivity(Payroll)

            }else if (position == 2){
                val ReportPayroll = Intent(this@Dashboard_HRD, ReportPayroll::class.java)
                startActivity(ReportPayroll)

            }else if (position == 3){
                val Cuti = Intent(this@Dashboard_HRD, Cuti_HRD::class.java)
                startActivity(Cuti)

            }else if (position == 4){
                val Status = Intent(this@Dashboard_HRD, StatusApproval::class.java)
                startActivity(Status)
            }
        }


    }

    inner class AsyncTaskHandleJson : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {

            var text:String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                text = connection.inputStream.use{it.reader().use{ reader->reader.readText()}}
            }finally {
                connection.disconnect();
            }
            return text
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handleJson(result)
        }
    }
    private fun handleJson(jsonString: String?){
        val jsonArray=  JSONArray(jsonString)
        var x = 0

        while(x<jsonArray.length()){
            val jsonObject = jsonArray.getJSONObject(x)
            tvUsernameHRd.setText(jsonObject.getString("nama"))
            tvPosisi.setText(jsonObject.getString("jabatan"))
            x++
        }
    }


    inner class AsyncTaskHandleJsonChartKaryawan : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {

            var text:String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                text = connection.inputStream.use{it.reader().use{ reader->reader.readText()}}
            }finally {
                connection.disconnect();
            }
            return text
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handleJsonKaryawan(result)
        }
    }
    private fun handleJsonKaryawan(jsonString: String?){
        val jsonArray=  JSONArray(jsonString)
        var x = 0

        while(x<jsonArray.length()){
            val jsonObject = jsonArray.getJSONObject(x)
            tvR.setText(jsonObject.getString("jmlKaryawan"))
            //tvR.setText(Integer.toString(34));

            lateinit var string1: String
            string1 = tvR.text.toString()

            var date1 = string1.toInt()
            pieChart.addPieSlice(
                PieModel(
                    "R", date1.toFloat(),
                    Color.parseColor("#FFA726")
                )
            )
            x++
        }
    }

    //get All HRD
    inner class AsyncTaskHandleJsonHRD : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {

            var text:String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                text = connection.inputStream.use{it.reader().use{ reader->reader.readText()}}
            }finally {
                connection.disconnect();
            }
            return text
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handleJsonHRD(result)
        }
    }
    private fun handleJsonHRD(jsonString: String?){
        val jsonArray=  JSONArray(jsonString)
        var x = 0

        while(x<jsonArray.length()){
            val jsonObject = jsonArray.getJSONObject(x)
            tvPython.setText(jsonObject.getString("jmlHRD"))


            pieChart.addPieSlice(
                PieModel(
                    "Python", tvPython.text.toString().toInt().toFloat(),
                    Color.parseColor("#66BB6A")
                )
            )
            x++
        }
    }


}
