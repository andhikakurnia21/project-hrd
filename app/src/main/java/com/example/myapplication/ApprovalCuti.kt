package com.example.myapplication

import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONException
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class ApprovalCuti : AppCompatActivity() {

    lateinit var textView15 : TextView
    lateinit var textView17 : TextView
    lateinit var textView18 : TextView
    lateinit var textView30 : TextView
    lateinit var textView32 : TextView
    lateinit var textView36 : TextView
    lateinit var btnTolakCuti : Button
    lateinit var btnSetujuCuti : Button
    lateinit var session: Session
    var sisa = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_approval_cuti)
        session = Session(applicationContext)



        textView15 = findViewById(R.id.textView15)
        textView17 = findViewById(R.id.textView17)
        textView18 = findViewById(R.id.textView18)
        textView30 = findViewById(R.id.textView30)
        textView32 = findViewById(R.id.textView32)
        textView36 = findViewById(R.id.textView36)
        btnTolakCuti = findViewById(R.id.btnTolakCuti)
        btnSetujuCuti = findViewById(R.id.btnSetujuCuti)



        //Get Param Dari Intent Approval
        val id = intent.getStringExtra("id_cuti")
        val username = intent.getStringExtra("username")
        val startDate = intent.getStringExtra("start_date")
        val endDate = intent.getStringExtra("end_date")
        val sisa_cuti = intent.getStringExtra("sisa_cuti")


        val url = "http://rs-webservice.xyz/index.php/User_HRD/get_total/"+id

        textView30.setText(id)
        textView15.setText(username)
        textView17.setText(startDate)
        textView18.setText(endDate)
        textView36.setText(sisa_cuti)

        //Selisih Hari End-Start
        //Perhitungan selisih tanggal
        lateinit var string1: String
        lateinit var string2: String
        string1 = textView17.text.toString().replace("/", "-")
        string2 = textView18.text.toString().replace("/", "-")
        var date1 = LocalDate.parse(string1)
        var date2 = LocalDate.parse(string2)
        val dates1: Date
        val dates2: Date
        val dfd = SimpleDateFormat("yyyy-MM-dd")
        dates1 = dfd.parse(date1.toString())
        dates2 = dfd.parse(date2.toString())
        var difference: Long = (dates2.time - dates1.time)
        var differenceDates = difference / (24 * 60 * 60 * 1000)
        var tmp = differenceDates+1
        sisa = (sisa_cuti.toInt() - tmp).toInt()
        val dayDifference = java.lang.Long.toString(tmp)
        var tmpday = dayDifference.toString()
        textView32.setText(tmpday)




        btnTolakCuti.setOnClickListener {
            UpdateStatus().execute()

        }

        btnSetujuCuti.setOnClickListener {
           if (sisa<0){
               Toast.makeText(this@ApprovalCuti, "Jatah Cuti Tidak Mencukupi", Toast.LENGTH_LONG).show()
           }else{
               SetujuCuti().execute()
               AsyncTaskHandleJson().execute(url)
           }
        }
    }

    inner class UpdateStatus : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {
            val requestQueue = Volley.newRequestQueue(this@ApprovalCuti)
            val stringRequest: StringRequest = object : StringRequest(
                Method.POST,
                getString(R.string.url).toString() + "User_HRD/update_status",
                Response.Listener { response ->
                    if (response.equals("[]", ignoreCase = true)) {
                    } else {
                        // Process response
                        try {
                            val jsonArray = JSONArray(response)
                            val jsonObject = jsonArray.getJSONObject(0)

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("VOLLEY", error.toString())
                })
            {
                override fun getParams(): Map<String, String>? {
                    val params: MutableMap<String, String> = HashMap()
                    params["idCuti"] = textView30.text.toString()
                    return params
                }
            }
            requestQueue.add(stringRequest)
            return ""
        }
        override fun onPostExecute(s: String?) {
            super.onPostExecute(s)
            val BckApproval = Intent(this@ApprovalCuti, Approval::class.java)
            startActivity(BckApproval)
            finish()
        }
    }



    inner class SetujuCuti : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {
            val requestQueue = Volley.newRequestQueue(this@ApprovalCuti)
            val stringRequest: StringRequest = object : StringRequest(
                Method.POST,
                getString(R.string.url).toString() + "User_HRD/setuju/",
                Response.Listener { response ->
                    if (response.equals("[]", ignoreCase = true)) {
                    } else {
                        // Process response
                        try {
                            val jsonArray = JSONArray(response)
                            jsonArray.getJSONObject(0)

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("VOLLEY", error.toString())
                })
            {

                override fun getParams(): Map<String, String>? {
                    val params: MutableMap<String, String> = HashMap()
                    params["id_cuti"] = textView30.text.toString()
                    params["user"] = textView15.text.toString()
                    params["id_approve"] = session.getUserDetails().get("userID").toString()
                    return params
                }
            }
            requestQueue.add(stringRequest)
            return ""
        }
        override fun onPostExecute(s: String?) {
            super.onPostExecute(s)
            val BckApproval = Intent(this@ApprovalCuti, Approval::class.java)
            startActivity(BckApproval)
            finish()
        }
    }


    inner class AsyncTaskHandleJson : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {

            var text:String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                text = connection.inputStream.use{it.reader().use{ reader->reader.readText()}}
            }finally {
                connection.disconnect();
            }
            return text
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handleJson(result)
        }
    }
    private fun handleJson(jsonString: String?){
        val jsonArray=  JSONArray(jsonString)
        var x = 0

        while(x<jsonArray.length()){
            val jsonObject = jsonArray.getJSONObject(x)
            x++
        }
    }
}
