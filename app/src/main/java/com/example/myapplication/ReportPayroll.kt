package com.example.myapplication

import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.myapplication.payroll.DataPayroll
import com.example.myapplication.payroll.Payroll
import org.json.JSONArray
import org.json.JSONException
import java.net.HttpURLConnection
import java.net.URL
import java.text.NumberFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class ReportPayroll : AppCompatActivity() {

    lateinit var btnCancelPayroll: Button
    lateinit var btnNextPayroll: Button
    lateinit var spinnerTahun: Spinner
    lateinit var spinnerBulan: Spinner
    lateinit var tableDataPayroll: TableLayout

    val currentDateTime = LocalDateTime.now()
    var year = ArrayList<String>()
    var tmpUser = ArrayList<String>()
    private var dataPayroll: MutableList<DataPayroll> = mutableListOf()
    private var payroll: Payroll? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report_payroll)
        setTitle("Report Payroll")
        payroll = Payroll()

        //inisialize
        btnCancelPayroll = findViewById(R.id.btnCancelPayroll);
        btnNextPayroll = findViewById(R.id.btnNextPayroll);
        spinnerTahun = findViewById(R.id.spinnerTahun);
        spinnerBulan = findViewById(R.id.spinnerBulan);
        tableDataPayroll = findViewById(R.id.tableDataPayroll);


        //spinner Tahun
        var tmpyear = currentDateTime.format(DateTimeFormatter.ofPattern("yyyy"))
        var yearNow = tmpyear.toInt()
        var start = yearNow-5
        for (i in start..yearNow ){
            year.add(i.toString())
        }
        var tahun = ArrayAdapter(this, android.R.layout.simple_spinner_item, year)
        tahun.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerTahun.setAdapter(tahun)

        //Spinner Bulan
        var month = arrayOf(
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember"
        )
        var bulan = ArrayAdapter(this, android.R.layout.simple_spinner_item, month)
        bulan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerBulan.setAdapter(bulan)

        btnCancelPayroll.setOnClickListener {
            finish()
        }
        val urlPayroll = "http://rs-webservice.xyz/index.php/User_HRD/all_data_payroll/"
        AsyncTaskHandleJsonDataPayroll().execute(urlPayroll)

        btnNextPayroll.setOnClickListener {
            filterPayroll().execute()
        }
        //loadHasilPayroll()
    }

    //table hasil Payroll
    fun loadHasilPayroll() {
        val leftRowMargin = 0
        val topRowMargin = 0
        val rightRowMargin = 0
        val bottomRowMargin = 0

        val rows: Int = dataPayroll.size
        var textSpacer: TextView? = null
        tableDataPayroll.removeAllViews()
        // -1 means heading row
        for (i in -1 until rows) {
            var row: DataPayroll? = null
            if (i > -1) {
                row = dataPayroll.get(i)
            } else {
                textSpacer = TextView(this)
                textSpacer.text = ""
            }
            //data columns
            val tv = TextView(this)
            tv.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT
            )
            tv.gravity = Gravity.START
            tv.setPadding(25, 15, 50, 25)
            if (i == -1) {
                tv.setBackgroundColor(Color.parseColor("#f0f0f0"))
                tv.text = "Employee"

            } else {
                tv.setBackgroundColor(Color.parseColor("#f8f8f8"))
                if (row != null) {
                    val gaji_pokok = row.gaji_pokok
                    var tmp_gaji_pokok = Integer.parseInt(gaji_pokok);
                    val Total = formatRupiah((tmp_gaji_pokok.toDouble()))
                    tv.setText(row.nama + "\n" + Total + "\n" + row.id_payroll);
                }
            }
            val tv2 = TextView(this)
            if (i == -1) {
                tv2.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            } else {
                tv2.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            }
            tv2.gravity = Gravity.START
            tv2.setPadding(25, 15, 50, 25)
            if (i == -1) {
                tv2.text = "Allowance"
                tv2.setBackgroundColor(Color.parseColor("#f0f0f0"))
            } else {
                tv2.setBackgroundColor(Color.parseColor("#f8f8f8"))
                if (row != null) {

                    val lain_lain = row.lain_lain_UK
                    var tmp_lain_lain = Integer.parseInt(lain_lain);

                    val Allowance = formatRupiah((tmp_lain_lain.toDouble()))
                    tv2.setText("\n" + Allowance);
                }
            }
            val tv3 = TextView(this)
            if (i == -1) {
                tv3.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            } else {
                tv3.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            }
            tv3.gravity = Gravity.START
            tv3.setPadding(25, 15, 50, 25)
            if (i == -1) {
                tv3.text = "Additional Earnings"
                tv3.setBackgroundColor(Color.parseColor("#f0f0f0"))
            } else {
                tv3.setBackgroundColor(Color.parseColor("#f8f8f8"))
                if (row != null) {


                    val uang_lembur_regular = row.uang_lembur_regular
                    val uang_lembur_libur = row.uang_lembur_hari_libur
                    val uang_kerajinan = row.uang_kerajinan
                    val uang_makan = row.uang_makan


                    var tmp_uang_lembur_regular = Integer.parseInt(uang_lembur_regular);
                    var tmp_uang_lembur_libur = Integer.parseInt(uang_lembur_libur);
                    var tmp_uang_kerajinan = Integer.parseInt(uang_kerajinan);
                    var tmp_uang_makan = Integer.parseInt(uang_makan);

                    val Additional = formatRupiah(tmp_uang_lembur_regular.toDouble() + tmp_uang_lembur_libur.toDouble()+tmp_uang_kerajinan.toDouble()+tmp_uang_makan.toDouble())
                    tv3.setText("\n" + Additional);
                }
            }
            val tv4 = TextView(this)
            if (i == -1) {
                tv4.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            } else {
                tv4.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            }
            tv4.gravity = Gravity.START
            tv4.setPadding(25, 15, 25, 15)
            if (i == -1) {
                tv4.text = "Deduction"
                tv4.setBackgroundColor(Color.parseColor("#f0f0f0"))
            } else {
                tv4.setBackgroundColor(Color.parseColor("#f8f8f8"))
                tv4.setTextColor(Color.parseColor("#ff0000"))
                if (row != null) {

                    val terlambat = row.terlambat
                    val pinjaman = row.pinjaman
                    val iuran_bpjs = row.iuran_bpjs


                    var tmp_terlambat = Integer.parseInt(terlambat);
                    var tmp_pinjaman = Integer.parseInt(pinjaman);
                    var tmp_iuran_bpjs = Integer.parseInt(iuran_bpjs);

                    val Deduction = formatRupiah((tmp_terlambat.toDouble() + tmp_pinjaman.toDouble() + tmp_iuran_bpjs.toDouble()))
                    tv4.setText("\n" + Deduction);
                }
            }
            val tv5 = TextView(this)
            if (i == -1) {
                tv5.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            } else {
                tv5.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            }
            tv5.gravity = Gravity.START
            tv5.setPadding(25, 15, 25, 15)
            if (i == -1) {
                tv5.text = "Total Pay"
                tv5.setBackgroundColor(Color.parseColor("#f0f0f0"))
            } else {
                tv5.setBackgroundColor(Color.parseColor("#f8f8f8"))
                tv5.setTextColor(Color.parseColor("#228B22"))
                if (row != null) {

                    //Additional
                    val gaji_pokok = row.gaji_pokok
                    val uang_lembur_regular = row.uang_lembur_regular
                    val uang_lembur_libur = row.uang_lembur_hari_libur
                    val lain_lain = row.lain_lain_UK
                    val uang_kerajinan = row.uang_kerajinan
                    val uang_makan = row.uang_makan

                    var tmp_gaji_pokok = Integer.parseInt(gaji_pokok);
                    var tmp_uang_lembur_regular = Integer.parseInt(uang_lembur_regular);
                    var tmp_uang_lembur_libur = Integer.parseInt(uang_lembur_libur);
                    var tmp_lain_lain = Integer.parseInt(lain_lain);
                    var tmp_uang_kerajinan = Integer.parseInt(uang_kerajinan);
                    var tmp_uang_makan = Integer.parseInt(uang_makan);

                    //Deduction
                    val terlambat = row.terlambat
                    val pinjaman = row.pinjaman
                    val iuran_bpjs = row.iuran_bpjs
                    var tmp_terlambat = Integer.parseInt(terlambat);
                    var tmp_pinjaman = Integer.parseInt(pinjaman);
                    var tmp_iuran_bpjs = Integer.parseInt(iuran_bpjs);

                    val Total = formatRupiah((tmp_gaji_pokok.toDouble()  + tmp_uang_lembur_regular.toDouble() + tmp_uang_lembur_libur.toDouble() + tmp_lain_lain.toDouble()+tmp_uang_kerajinan.toDouble()+tmp_uang_makan.toDouble()) - (tmp_terlambat.toDouble() + tmp_pinjaman.toDouble() + tmp_iuran_bpjs.toDouble()))
                    tv5.setText("\n" + Total);
                }
            }

            // add table row
            val tr = TableRow(this)
            tr.id = i + 1
            val trParams = TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT
            )
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin)
            tr.setPadding(5, 5, 5, 5)
            tr.layoutParams = trParams
            tr.addView(tv)
            tr.addView(tv2)
            tr.addView(tv3)
            tr.addView(tv4)
            tr.addView(tv5)
            if (i > -1) {
                tr.setOnClickListener {
                    val tvEmployee = tr.getChildAt(0) as TextView
                    val UpdatePayroll = Intent(this@ReportPayroll, UpdateDataPayroll::class.java)
                    UpdatePayroll.putExtra("karyawan", tvEmployee.text.toString())
                    startActivity(UpdatePayroll)
                }
            }
            tableDataPayroll.addView(tr, trParams)
            if (i > -1) {
                // add separator row
                val trSep = TableRow(this)
                val trParamsSep = TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT
                )
                trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin)
                trSep.layoutParams = trParamsSep
                val tvSep = TextView(this)
                val tvSepLay = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT
                )
                tvSepLay.span = 4
                tvSep.layoutParams = tvSepLay
                tvSep.setBackgroundColor(Color.parseColor("#d9d9d9"))
                tvSep.height = 1
                trSep.addView(tvSep)
                tableDataPayroll.addView(trSep, trParamsSep)
            }
        }
    }

    //Data Payroll
    inner class AsyncTaskHandleJsonDataPayroll : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {

            var text:String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                text = connection.inputStream.use{it.reader().use{ reader->reader.readText()}}
            }finally {
                connection.disconnect();
            }
            return text
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handleJsonDataPayroll(result)
        }
    }
    private fun handleJsonDataPayroll(jsonString: String?){
        dataPayroll.clear()
        dataPayroll = payroll?.getDataPayroll(jsonString)!!
        loadHasilPayroll()
    }


    inner class filterPayroll: AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {
            val requestQueue = Volley.newRequestQueue(this@ReportPayroll)
            val stringRequest: StringRequest = object : StringRequest(
                Method.POST,
                getString(R.string.url).toString() + "User_HRD/filter_bulan_tahun",
                Response.Listener { response ->
                    if (response.equals("[]", ignoreCase = true)) {
                    } else {
                        // Process response
                        Log.i("aaaaaaaaaa", response)
                        try {
                            val jsonArray = JSONArray(response)
                            jsonArray.getJSONObject(0)

                            dataPayroll.clear()
                            dataPayroll = payroll?.getDataPayroll(response)!!
                            loadHasilPayroll()

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("VOLLEY", error.toString())
                })
            {
                override fun getParams(): Map<String, String>? {
                    val params: MutableMap<String, String> = HashMap()
                    params["filterBulanTahun"] = spinnerBulan.getSelectedItem().toString()+" "+spinnerTahun.getSelectedItem().toString()
                    return params
                }
            }
            requestQueue.add(stringRequest)
            return ""
        }
    }

    //format rupiah
    private fun formatRupiah(number: Double): String? {
        val localeID = Locale("in", "ID")
        val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
        return formatRupiah.format(number)
    }
}
