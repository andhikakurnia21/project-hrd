package com.example.myapplication.statusApproval

class DataStatusApproval {
    var id_cuti: String? = null
    var id_user: String? = null
    var start_date: String? = null
    var end_date: String? = null
    var tgl_approved: String? = null
    var hrd_approved: String? = null
    var status: String? = null
    var tgl_pengajuan: String? = null
}