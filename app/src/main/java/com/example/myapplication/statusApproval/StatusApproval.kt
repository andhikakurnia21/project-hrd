package com.example.myapplication.statusApproval

import org.json.JSONArray
import org.json.JSONException

class StatusApproval {
    @Throws(JSONException::class)
    fun getDataStatusApproval(response: String?): ArrayList<DataStatusApproval>? {
        val jsonArray = JSONArray(response)
        val data: ArrayList<DataStatusApproval> = ArrayList<DataStatusApproval>()
        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val row = DataStatusApproval()
            row.id_cuti = (jsonObject.getString("id_cuti"))
            row.id_user = (jsonObject.getString("id_user"))
            row.start_date = (jsonObject.getString("start_date"))
            row.end_date = (jsonObject.getString("end_date"))
            row.tgl_approved = (jsonObject.getString("tgl_approved"))
            row.hrd_approved = (jsonObject.getString("hrd_approved"))
            row.status = (jsonObject.getString("status"))
            row.tgl_pengajuan = (jsonObject.getString("tgl_pengajuan"))
            data.add(row)
        }
        return data
    }
}