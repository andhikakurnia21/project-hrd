package com.example.myapplication

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_custom_toast.*
import kotlinx.android.synthetic.main.activity_dashbord.*
import kotlinx.android.synthetic.main.activity_header.view.*
import kotlinx.android.synthetic.main.alert_dialog.view.*
import kotlinx.android.synthetic.main.alert_dialog_cuti.view.*
import org.json.JSONArray
import org.json.JSONException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList


class Dashbord_Karyawan : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var edStart: EditText
    lateinit var edEnd: EditText
    lateinit var btnAjukan:Button
    lateinit var btnTambah:Button
    lateinit var arrayAdapter: ArrayAdapter<String>
    lateinit var session: Session
    lateinit var sessionCalendar: SessionCalendar
    lateinit var listView: ListView
    private lateinit var navbar: NavigationView
    var list: ArrayList<String> = ArrayList()
    val calendar = Calendar.getInstance()
    var ctr = 0
    var listposition = 0
    val myFormatStart = "yyyy/MM/dd"
    val myFormatEnd = "yyyy/MM/dd"

    private lateinit var mToggle : ActionBarDrawerToggle

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashbord)
        setTitle("Dasboard")
        session = Session(applicationContext)
        sessionCalendar = SessionCalendar(applicationContext)

        //custom layout
        val customLayout = layoutInflater.inflate(R.layout.activity_custom_toast_submit, constraintlayout)

        //adapter
        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, list)

        // memunculkan tombol burger menu
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //////MENU BAR\\\\\\\\
        // untuk toggle open dan close navigation
        mToggle = ActionBarDrawerToggle(this, drawer_layout, R.string.Open, R.string.Close)
        // tambahkan mToggle ke drawer_layout sebagai pengendali open dan close drawer
        drawer_layout.addDrawerListener(mToggle)
        mToggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)

        //Parsing Variabel
        navbar = findViewById(R.id.nav_view)
        session.getUserDetails().get("userName")
        val nama = navbar.getHeaderView(0)
        val email = navbar.getHeaderView(0)
        val image = navbar.getHeaderView(0)
        nama.tvNama.text= session.getUserDetails().get("userName")
        email.tvEmail.text=session.getUserDetails().get("userEmail")
        image.imageProfile.setOnClickListener {
            val Profile = Intent(applicationContext, Profile::class.java)
            startActivity(Profile)
        }

        //Session
        session.checkLogin()
        var user:HashMap<String, String> = session.getUserDetails()
        var name:String  = user.get(Session.KEY_NAME)!!
        var position:String = user.get(Session.KEY_POSITION)!!


        //Initialize
        listView = findViewById(R.id.listView)
        edStart = findViewById(R.id.edStart)
        edEnd = findViewById(R.id.edEnd)
        btnAjukan = findViewById(R.id.btnAjukan)
        btnTambah = findViewById(R.id.btnTambah)
        listView = findViewById(R.id.listView)


        listView.setOnItemClickListener { parent, view, position, id ->
            listposition = position

            //Show Dialog
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.alert_dialog, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Hapus Tanggal Cuti")
            val mAlertDialog = mBuilder.show()

            mDialogView.dialogSubmitBtn.setOnClickListener {
                val posisi = listposition
                list.removeAt(posisi)
                listView.adapter = arrayAdapter
                arrayAdapter.notifyDataSetChanged()
                val jsonCalendar = Gson().toJson(list)
                sessionCalendar.saveCalendar(jsonCalendar)
                mAlertDialog.dismiss()
            }
            mDialogView.dialogCancelBtn.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }


        btnTambah.setOnClickListener {
            if(edStart.text.toString() != "" && edEnd.text.toString() != "") {
                listView.adapter = arrayAdapter
                arrayAdapter.notifyDataSetChanged()

                lateinit var string1: String
                lateinit var string2: String
                string1 = edStart.text.toString().replace("/", "-")
                string2 = edEnd.text.toString().replace("/", "-")
                var date1 = LocalDate.parse(string1)
                var date2 = LocalDate.parse(string2)
                val dates1: Date
                val dates2: Date
                val dfd = SimpleDateFormat("yyyy-MM-dd")
                dates1 = dfd.parse(date1.toString())
                dates2 = dfd.parse(date2.toString())
                var difference: Long = (dates2.time - dates1.time)
                var differenceDates = difference / (24 * 60 * 60 * 1000)
                val dayDifference = java.lang.Long.toString(differenceDates)

                if(differenceDates<0){
                    Toast.makeText(
                        this@Dashbord_Karyawan,
                        "Pastikan tanggal ambil cuti lebih kecil dari tanggal masuk",
                        Toast.LENGTH_LONG
                    ).show()
                }else if(dayDifference.equals("0")){
                    Toast.makeText(
                        this@Dashbord_Karyawan,
                        "Tanggal pengambilan cuti sama dengan tanggal masuk",
                        Toast.LENGTH_LONG
                    ).show()
                }else{

                    var found: Boolean? = null
                    for (i in list.indices) {
                        val selectedIndex = list[i]
                        if(selectedIndex.contains(edStart.text.toString())&&selectedIndex.contains(edEnd.text.toString())){
                            found = true
                            break
                        }
                    }

                    if (list.size <= 0){
                        var difference: Long = (dates2.time - dates1.time)
                        var differenceDates = difference / (24 * 60 * 60 * 1000)
                        var tmp = differenceDates+1
                        val dayDifference = java.lang.Long.toString(tmp)
                        list.add(edStart.text.toString() + " - " + edEnd.text.toString())
                        val jsonCalendar = Gson().toJson(list)
                        sessionCalendar.saveCalendar(jsonCalendar)
                        Log.i("asdasd",sessionCalendar.getDateDetails())
                    }else {
                        if (found == true){
                        }else {
                            var difference: Long = (dates2.time - dates1.time)
                            var differenceDates = difference / (24 * 60 * 60 * 1000)
                            var tmp = differenceDates+1
                            val dayDifference = java.lang.Long.toString(tmp)

                            list.add(edStart.text.toString() + " - " + edEnd.text.toString())
                            val jsonCalendar = Gson().toJson(list)
                            sessionCalendar.saveCalendar(jsonCalendar)
                        }
                    }
                }
            }else{
                Toast.makeText(
                    this@Dashbord_Karyawan,
                    "Tanggal pengambilan cuti dan tanggal masuk kosong",
                    Toast.LENGTH_LONG
                ).show()
            }
            edStart.setText("")
            edEnd.setText("")
        }


        /*if(sessionCalendar.isDataExist()){
            val jsonArray = JSONArray(sessionCalendar.getDateDetails())
            list.clear()
            for (i in 0 until jsonArray.length()) {
                val jsonObject: String = jsonArray.getString(i)
                list.add(jsonObject)
            }
            listView.adapter = arrayAdapter
            arrayAdapter.notifyDataSetChanged()
        }*/

        //Pengajuan Tanggal Cuti
        btnAjukan.setOnClickListener {
            if(list.size <=0){
                Toast.makeText(
                    this@Dashbord_Karyawan,
                    "Jadwal cuti masih kosong",
                    Toast.LENGTH_LONG
                ).show()
            }else{
                //Show Dialog
                val mDialogView = LayoutInflater.from(this).inflate(R.layout.alert_dialog_cuti, null)
                val mBuilder = AlertDialog.Builder(this)
                    .setView(mDialogView)
                    .setTitle("Pengajuan Tanggal Cuti")
                val mAlertDialog = mBuilder.show()

                mDialogView.btnSetujuAjukan.setOnClickListener {
                    val toast = Toast(this)
                    toast.duration = Toast.LENGTH_SHORT
                    toast.setGravity(Gravity.TOP, 0, 0)
                    toast.view = customLayout
                    toast.show()
                    insertTanggalCuti().execute()
                    mAlertDialog.dismiss()
                    list.clear()
                    arrayAdapter.notifyDataSetChanged()
                }
                mDialogView.btnBatalAjukan.setOnClickListener {
                    mAlertDialog.dismiss()
                }
            }
        }

        //Calendar
        val dateStart =
            OnDateSetListener { view, year, monthOfYear, dayOfMonth -> // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateLabelStart()
            }
        val dateEnd =
            OnDateSetListener { view, year, monthOfYear, dayOfMonth -> // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateLabelEnd()
            }

        edStart.setOnClickListener {
            DatePickerDialog(
                this@Dashbord_Karyawan, dateStart, calendar.get(Calendar.YEAR), calendar.get(
                    Calendar.MONTH
                ), calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
        edEnd.setOnClickListener {
            DatePickerDialog(
                this@Dashbord_Karyawan, dateEnd, calendar.get(Calendar.YEAR), calendar.get(
                    Calendar.MONTH
                ), calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
    }

    private fun updateLabelStart() {
        val sdf = SimpleDateFormat(myFormatStart, Locale.US)
        edStart.setText(sdf.format(calendar.getTime()))
    }
    private fun updateLabelEnd() {
        val sdf = SimpleDateFormat(myFormatEnd, Locale.US)
        edEnd.setText(sdf.format(calendar.getTime()))
    }


    //buat icon notif
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return mToggle.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
         when (item.itemId) {
             R.id.profile -> {
                 val Profile = Intent(applicationContext, Profile::class.java)
                 startActivity(Profile)
             }
             R.id.statusApproval -> {
                 val statusApproval = Intent(applicationContext, StatusApproval::class.java)
                 startActivity(statusApproval)
             }
             R.id.logout -> {
                 val Logout = Intent(applicationContext, MainActivity::class.java)
                 startActivity(Logout)
                 session.logoutUser()
                 sessionCalendar.clearSession()
                 finish()
             }
            else -> true
        }
        return true
    }


    inner class insertTanggalCuti : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {
            val requestQueue = Volley.newRequestQueue(this@Dashbord_Karyawan)
            val stringRequest: StringRequest = object : StringRequest(
                Method.POST,
                getString(R.string.url).toString() + "User_HRD/insert_tanggal_cuti",
                Response.Listener { response ->
                    if (response.equals("[]", ignoreCase = true)) {
                    } else {
                        // Process response
                        try {
                            val jsonArray = JSONArray(response)
                            val jsonObject = jsonArray.getJSONObject(0)

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("VOLLEY", error.toString())
                })
            {
                override fun getParams(): Map<String, String>? {
                    val params: MutableMap<String, String> = HashMap()
                    var tanggal = String()
                    if (sessionCalendar.getDateDetails() != null) {
                        tanggal = sessionCalendar.getDateDetails()
                    }
                    params["id"] = session.getUserDetails().get("userID").toString()
                    params["tanggal"] = tanggal
                    return params
                }
            }
            requestQueue.add(stringRequest)
            return ""
        }
    }
}
