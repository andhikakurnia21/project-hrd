package com.example.myapplication

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences

class Session {
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var con:Context
    var PRIVATE_MODE:Int = 0

    constructor(con: Context){
        this.con = con
        pref = con.getSharedPreferences(PREF_NAME,PRIVATE_MODE)
        editor = pref.edit()
    }

    companion object{
        val PREF_NAME: String = "com.example.myapplication"
        val IS_LOGIN: String = "isLoggedIn"
        val KEY_NAME: String = "userName"
        val KEY_ID: String = "userID"
        val KEY_EMAIL: String = "userEmail"
        val KEY_POSITION: String = "userPosition"
    }

    fun createLoginSession(userId:String,userName:String,userPosition:String, userEmail:String ){
        editor.putBoolean(IS_LOGIN,true)
        editor.putString(KEY_ID,userId)
        editor.putString(KEY_NAME,userName)
        editor.putString(KEY_EMAIL,userEmail)
        editor.putString(KEY_POSITION,userPosition)
        editor.commit()
    }

    fun checkLogin()
    {
        if(!this.isLoggedIn())
        {
          /* var i: Intent = Intent(con, MainActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            con.startActivities(arrayOf(i))*/
        }
    }

    fun getUserDetails():HashMap<String,String>
    {
        var user: Map<String,String> = HashMap<String,String>()
        (user as HashMap).put(KEY_ID,pref.getString(KEY_ID,null))
        (user as HashMap).put(KEY_NAME,pref.getString(KEY_NAME,null))
        (user as HashMap).put(KEY_EMAIL,pref.getString(KEY_EMAIL,null))
        (user as HashMap).put(KEY_POSITION,pref.getString(KEY_POSITION,null))
        return user
    }

    fun logoutUser()
    {
        editor.clear()
        editor.commit()

    }

    fun isLoggedIn():Boolean{
        return pref.getBoolean(IS_LOGIN,false)
    }
}