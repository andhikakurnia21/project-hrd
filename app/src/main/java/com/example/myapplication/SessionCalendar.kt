package com.example.myapplication

import android.content.Context
import android.content.SharedPreferences

class SessionCalendar {
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var con: Context
    var PRIVATE_MODE:Int = 0

    constructor(con: Context){
        this.con = con
        pref = con.getSharedPreferences(PREF_NAME,PRIVATE_MODE)
        editor = pref.edit()
    }

    companion object{
        val PREF_NAME: String = "com.example.myapplication"
        val IS_LOGIN: String = "isLoggedIn"
        val KEY_DATE: String = "tanggal"
    }


    fun saveCalendar(tanggal:String ){
        editor.putBoolean(IS_LOGIN,true)
        editor.putString(KEY_DATE,tanggal)
        editor.commit()
    }



    fun getDateDetails():String
    {
        return pref.getString(KEY_DATE,"")
    }

    fun clearSession()
    {
        editor.clear()
        editor.commit()

    }

   fun isDataExist():Boolean{
        return pref.getBoolean(IS_LOGIN,false)
    }
}