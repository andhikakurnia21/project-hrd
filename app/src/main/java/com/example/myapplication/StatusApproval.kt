package com.example.myapplication

import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.view.Gravity
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.statusApproval.DataStatusApproval
import com.example.myapplication.statusApproval.StatusApproval
import java.net.HttpURLConnection
import java.net.URL

class StatusApproval : AppCompatActivity() {
    lateinit var session: Session
    lateinit var statusCuti: TableLayout
    private var dataStatusApproval: MutableList<DataStatusApproval> = mutableListOf()
    private var statusApproval: StatusApproval? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status_approval)
        supportActionBar?.hide()
        session = Session(applicationContext)
        statusCuti = findViewById(R.id.statusCuti)
        statusApproval = StatusApproval()
        val urlStatus = "http://rs-webservice.xyz/index.php/User_HRD/all_data_status_approval/"+session.getUserDetails().get(
            "userID"
        )
        AsyncTaskHandleJsonStatusApproval().execute(urlStatus)
    }

    //Data Payroll
    inner class AsyncTaskHandleJsonStatusApproval : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {

            var text:String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                text = connection.inputStream.use{it.reader().use{ reader->reader.readText()}}
            }finally {
                connection.disconnect();
            }
            return text
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handleJsonStatusApproval(result)
        }
    }
    private fun handleJsonStatusApproval(jsonString: String?){
        dataStatusApproval.clear()
        dataStatusApproval = statusApproval?.getDataStatusApproval(jsonString)!!
        loadDataStatusApproval()
    }

    //table hasil Payroll
    fun loadDataStatusApproval() {
        val leftRowMargin = 0
        val topRowMargin = 0
        val rightRowMargin = 0
        val bottomRowMargin = 0

        val rows: Int = dataStatusApproval.size
        var textSpacer: TextView? = null
        statusCuti.removeAllViews()
        // -1 means heading row
        for (i in -1 until rows) {
            var row: DataStatusApproval? = null
            if (i > -1) {
                row = dataStatusApproval.get(i)
            } else {
                textSpacer = TextView(this)
                textSpacer.text = ""
            }
            // data columns
            val tv = TextView(this)
            tv.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT
            )
            tv.gravity = Gravity.START
            tv.setPadding(25, 15, 50, 15)
            if (i == -1) {
                tv.text = "Tgl Cuti"
                tv.setBackgroundColor(Color.parseColor("#f0f0f0"))
            } else {
                tv.setBackgroundColor(Color.parseColor("#f8f8f8"))
                if (row != null) {
                    tv.setText(row.start_date)
                }
            }
            val tv2 = TextView(this)
            if (i == -1) {
                tv2.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            } else {
                tv2.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            }
            tv2.gravity = Gravity.START
            tv2.setPadding(25, 15, 50, 15)
            if (i == -1) {
                tv2.text = "Sampai Dengan"
                tv2.setBackgroundColor(Color.parseColor("#f0f0f0"))
            } else {
                tv2.setBackgroundColor(Color.parseColor("#f8f8f8"))
                if (row != null) {
                    tv2.setText(row.end_date);
                }
            }
            val tv3 = TextView(this)
            if (i == -1) {
                tv3.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            } else {
                tv3.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            }
            tv3.gravity = Gravity.START
            tv3.setPadding(25, 15, 50, 15)
            if (i == -1) {
                tv3.text = "Tgl Approved"
                tv3.setBackgroundColor(Color.parseColor("#f0f0f0"))
            } else {
                tv3.setBackgroundColor(Color.parseColor("#f8f8f8"))
                if (row != null) {
                    tv3.setText(row.tgl_approved);
                }
            }
            val tv4 = TextView(this)
            if (i == -1) {
                tv4.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            } else {
                tv4.layoutParams = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
            }
            tv4.gravity = Gravity.START
            tv4.setPadding(25, 15, 50, 15)
            if (i == -1) {
                tv4.text = "Status"
                tv4.setBackgroundColor(Color.parseColor("#f0f0f0"))
            } else {
                tv4.setBackgroundColor(Color.parseColor("#f8f8f8"))
                if (row != null) {
                    if(row.status.equals("1")){
                        tv4.setTextColor(Color.parseColor("#228B22"))
                        tv4.setText("Telah disetujui");
                    }else if(row.status.equals("-1")){
                        tv4.setTextColor(Color.parseColor("#ff0000"))
                        tv4.setText("Tidak disetujui");
                    }else{
                        tv4.setText("Belum disetujui");
                    }
                }
            }
            // add table row
            val tr = TableRow(this)
            tr.id = i + 1
            val trParams = TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT
            )
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin)
            tr.setPadding(5, 5, 5, 5)
            tr.layoutParams = trParams
            tr.addView(tv)
            tr.addView(tv2)
            tr.addView(tv3)
            tr.addView(tv4)
            if (i > -1) {

            }
            statusCuti.addView(tr, trParams)
            if (i > -1) {
                // add separator row
                val trSep = TableRow(this)
                val trParamsSep = TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.MATCH_PARENT
                )
                trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin)
                trSep.layoutParams = trParamsSep
                val tvSep = TextView(this)
                val tvSepLay = TableRow.LayoutParams(
                    TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT
                )
                tvSepLay.span = 4
                tvSep.layoutParams = tvSepLay
                tvSep.setBackgroundColor(Color.parseColor("#d9d9d9"))
                tvSep.height = 1
                trSep.addView(tvSep)
                statusCuti.addView(trSep, trParamsSep)
            }
        }
    }
}
