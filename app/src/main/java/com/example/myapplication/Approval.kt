package com.example.myapplication

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.AsyncTask
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONArray
import org.json.JSONException
import java.net.HttpURLConnection
import java.net.URL
import java.text.ParseException

class Approval : AppCompatActivity() {

    lateinit var linearLayoutGetAllCuti: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_approval)
        val urlGetAllCuti = "http://rs-webservice.xyz/index.php/User_HRD/detail_cuti_notification/"
        AsyncTaskGetAllCuti().execute(urlGetAllCuti)

        linearLayoutGetAllCuti = findViewById(R.id.linearLayoutCuti);
    }

    //get All HRD
    inner class AsyncTaskGetAllCuti : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {

            var text:String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                text = connection.inputStream.use{it.reader().use{ reader->reader.readText()}}
            }finally {
                connection.disconnect();
            }
            return text
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handleJsonHRD(result)
        }
    }
    private fun handleJsonHRD(jsonString: String?){
        setDisplay(jsonString)
    }

    @Throws(JSONException::class, ParseException::class)
    fun setDisplay(response: String?) {
        val jsonArray = JSONArray(response)
        for (i in 0 until jsonArray.length()) {
            val jsonDate = jsonArray.getJSONArray(i)
            for (j in 0 until jsonDate.length()) {
                val jsonObject = jsonDate.getJSONObject(j)
                val display = windowManager.defaultDisplay
                if (j == 0) {
                    val tvHeader = TextView(this@Approval)
                    tvHeader.text = jsonObject.getString("tgl_pengajuan").toUpperCase()
                    tvHeader.textSize = "16.0".toFloat()
                    tvHeader.setTextColor(Color.rgb(140, 140, 140))
                    tvHeader.setTypeface(tvHeader.typeface, Typeface.BOLD)
                    val params = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                    params.setMargins(25, 10, 10, 10)
                    tvHeader.layoutParams = params
                    val display = windowManager.defaultDisplay
                    val width = display.width
                    val divider = LinearLayout(this@Approval)
                    val parms =
                        LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT)
                    divider.layoutParams = parms
                    divider.setBackgroundColor(Color.rgb(242, 242, 242))
                    divider.addView(tvHeader)

                    //linearLayout.addView(tvHeader);
                    linearLayoutGetAllCuti.addView(divider)
                }
                val tvContentHeader1 = TextView(this@Approval)
                val tvContentHeader2 = TextView(this@Approval)
                val tvContentHeader3 = TextView(this@Approval)
                val tvContent1 = TextView(this@Approval)
                val tvContent2 = TextView(this@Approval)
                val tvContent3 = TextView(this@Approval)

                //Content Header
                tvContentHeader1.text =  "Nama"
                tvContentHeader1.setTypeface(Typeface.MONOSPACE,Typeface.BOLD)
                tvContentHeader1.textSize = "14.0".toFloat()
                tvContentHeader1.setTextColor(Color.BLACK)
                tvContentHeader2.text =  "Cuti Dari Tanggal"
                tvContentHeader2.setTypeface(Typeface.MONOSPACE,Typeface.BOLD)
                tvContentHeader2.textSize = "14.0".toFloat()
                tvContentHeader2.setTextColor(Color.BLACK)
                tvContentHeader3.text =  "Hingga Tanggal"
                tvContentHeader3.setTypeface(Typeface.MONOSPACE,Typeface.BOLD)
                tvContentHeader3.textSize = "14.0".toFloat()
                tvContentHeader3.setTextColor(Color.BLACK)
                //

                //Content
                tvContent1.text = jsonObject.getString("username")
                tvContent2.text = jsonObject.getString("start_date")
                tvContent3.text = jsonObject.getString("end_date")
                tvContent1.textSize = "14.0".toFloat()
                tvContent1.setTextColor(Color.BLACK)
                tvContent2.textSize = "14.0".toFloat()
                tvContent2.setTextColor(Color.BLACK)
                tvContent3.textSize = "14.0".toFloat()
                tvContent3.setTextColor(Color.BLACK)
                //

                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(25, 10, 0, 10)
                params.weight = 1.toFloat()
                params.width = display.width/2
                tvContent1.layoutParams = params
                tvContent2.layoutParams = params
                tvContent3.layoutParams = params


                val paramsHeader = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                paramsHeader.setMargins(25, 10, 0, 10)
                paramsHeader.weight = 1.toFloat()
                paramsHeader.width = display.width/2
                tvContentHeader1.layoutParams = paramsHeader
                tvContentHeader2.layoutParams = paramsHeader
                tvContentHeader3.layoutParams = paramsHeader

                //Divider
                val width = display.width/2
                val height = 5
                val divider = LinearLayout(this@Approval)
                val parms = LinearLayout.LayoutParams(display.width, height)
                divider.layoutParams = parms
                divider.setBackgroundColor(Color.rgb(230, 230, 230))

                //1
                val linearLayout1 = LinearLayout(this@Approval)
                linearLayout1.addView(tvContentHeader1)
                linearLayout1.addView(tvContent1)
                //2
                val linearLayout2 = LinearLayout(this@Approval)
                linearLayout2.addView(tvContentHeader2)
                linearLayout2.addView(tvContent2)
                //3
                val linearLayout3 = LinearLayout(this@Approval)
                linearLayout3.addView(tvContentHeader3)
                linearLayout3.addView(tvContent3)

                val linearLayouttmp = LinearLayout(this@Approval)
                linearLayouttmp.setOrientation(LinearLayout.VERTICAL);
                linearLayouttmp.addView(linearLayout1)
                linearLayouttmp.addView(linearLayout2)
                linearLayouttmp.addView(linearLayout3)


                linearLayoutGetAllCuti.addView(linearLayouttmp)
                linearLayoutGetAllCuti.addView(divider)



                linearLayouttmp.setOnClickListener {
                    val ApproveIdCuti = Intent(this@Approval, ApprovalCuti::class.java)
                    ApproveIdCuti.putExtra("id_cuti",jsonObject.getString("id_cuti"))
                    ApproveIdCuti.putExtra("username",jsonObject.getString("username"))
                    ApproveIdCuti.putExtra("start_date",jsonObject.getString("start_date"))
                    ApproveIdCuti.putExtra("end_date",jsonObject.getString("end_date"))
                    ApproveIdCuti.putExtra("sisa_cuti",jsonObject.getString("sisa_cuti"))
                    startActivity(ApproveIdCuti)
                    finish()
                }
            }
        }
    }
}