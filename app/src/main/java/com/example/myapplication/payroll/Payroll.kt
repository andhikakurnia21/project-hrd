package com.example.myapplication.payroll


import android.util.Log
import org.json.JSONArray
import org.json.JSONException
import java.util.*


class Payroll {
    @Throws(JSONException::class)
    fun getDataPayroll(response: String?): ArrayList<DataPayroll>? {
        val jsonArray = JSONArray(response)
        val data: ArrayList<DataPayroll> = ArrayList<DataPayroll>()
        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val row = DataPayroll()
            row.id = (jsonObject.getString("id"))
            row.nama = (jsonObject.getString("nama"))
            row.id_payroll = (jsonObject.getString("id_payroll"))
            row.id_karyawan = (jsonObject.getString("id_karyawan"))
            row.tgl_input_payroll = (jsonObject.getString("tgl_input_payroll"))
            row.bulan_payroll = (jsonObject.getString("bulan_payroll"))
            row.gaji_pokok = (jsonObject.getString("gaji_pokok"))
            row.attendence = (jsonObject.getString("attendence"))
            row.uang_lembur_regular = (jsonObject.getString("uang_lembur_regular"))
            row.uang_lembur_hari_libur = (jsonObject.getString("uang_lembur_hari_libur"))
            row.lain_lain_UK = (jsonObject.getString("lain_lain_UK"))
            row.terlambat = (jsonObject.getString("terlambat"))
            row.pinjaman = (jsonObject.getString("pinjaman"))
            row.iuran_bpjs = (jsonObject.getString("iuran_bpjs"))
            row.uang_kerajinan = (jsonObject.getString("uang_kerajinan"))
            row.uang_makan = (jsonObject.getString("uang_makan"))
            row.total_gaji = (jsonObject.getString("total_gaji"))
            data.add(row)
        }
        return data
    }
}