package com.example.myapplication.payroll

public class DataPayroll {

    var id: String? = null
    var nama: String? = null
    var id_payroll: String? = null
    var id_karyawan: String? = null
    var tgl_input_payroll: String? = null
    var bulan_payroll: String? = null
    var gaji_pokok: String? = null
    var attendence: String? = null
    var uang_lembur_regular: String? = null
    var uang_lembur_hari_libur: String? = null
    var lain_lain_UK: String? = null
    var terlambat: String? = null
    var pinjaman: String? = null
    var iuran_bpjs: String? = null
    var uang_kerajinan: String? = null
    var uang_makan: String? = null
    var total_gaji: String? = null
}