package com.example.myapplication

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_custom_toast.*
import kotlinx.android.synthetic.main.activity_update_data_payroll.*
import kotlinx.android.synthetic.main.alert_dialog_update_payroll.view.*
import org.json.JSONArray
import org.json.JSONException
import java.net.HttpURLConnection
import java.net.URL
import java.text.NumberFormat
import java.util.*
import kotlin.properties.Delegates

class UpdateDataPayroll : AppCompatActivity() {

    lateinit var tvID: TextView
    lateinit var tvNamaKaryawan: TextView

    lateinit var edUK: EditText
    lateinit var edUangMakan: EditText
    lateinit var edRelugar: EditText
    lateinit var edLibur: EditText
    lateinit var edTerlambat: EditText
    lateinit var edUpdatePinjaman: EditText
    lateinit var edUpdateIuran: EditText
    lateinit var edUangKerajinan: EditText

    lateinit var edUpdateLemburReguler: EditText
    lateinit var edUpdateLemburLibur: EditText
    lateinit var edUpdateTerlambat: EditText

    lateinit var btnUpdateDataPayroll: Button
    lateinit var btnTest: Button
    lateinit var tmpTotal: String
    lateinit var blnPayroll: String
    lateinit var tmpUpdateData: String
    lateinit var tmpIdPayroll: String

    lateinit var tmpLemburReguler: String
    lateinit var tmpLibur: String
    lateinit var tmpTerlambat: String

    lateinit var tmpStringLemburReguler: String
    lateinit var tmpStringLibur: String
    lateinit var tmpStringTerlambat: String


    //tmp Int dan Double string
    var tmpIntLemburReguler by Delegates.notNull<Int>()
    var tmpIntLemburLibur by Delegates.notNull<Int>()
    var tmpIntTerlambat by Delegates.notNull<Int>()

    var tmpDoubleLemburReguler by Delegates.notNull<Double>()
    var tmpDoubleLemburLibur by Delegates.notNull<Double>()
    var tmpDoubleTerlambat by Delegates.notNull<Double>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_data_payroll)
        setTitle("Update Data Payroll")

        //Initialize
        tvID = findViewById(R.id.tvID)
        tvNamaKaryawan = findViewById(R.id.tvNamaKaryawan)
        edUK = findViewById(R.id.edUK)
        edUangMakan = findViewById(R.id.edUangMakan)
        edUpdateLemburReguler = findViewById(R.id.edUpdateLemburReguler)
        edUpdateLemburLibur = findViewById(R.id.edUpdateLemburLibur)
        edRelugar = findViewById(R.id.edRegular)
        edLibur = findViewById(R.id.edLibur)
        btnUpdateDataPayroll = findViewById(R.id.btnUpdateDataPayroll)
        edTerlambat = findViewById(R.id.edTerlambat)
        edUpdateTerlambat = findViewById(R.id.edUpdateTerlambat)
        edUpdatePinjaman = findViewById(R.id.edUpdatePinjaman)
        edUpdateIuran = findViewById(R.id.edUpdateIuran)
        edUangKerajinan = findViewById(R.id.edUangKerajinan)


        //get Param from report Payroll
        val id = intent.getStringExtra("karyawan")
        val split = id.split("\n")
        tvNamaKaryawan.setText(split[0].trim())
        var id_payroll = split[2].trim()
        tmpIdPayroll = id_payroll

        //url gaji pokok karyawan
        val url = "http://rs-webservice.xyz/index.php/User_HRD/data_payroll/"+id_payroll
        AsyncTaskHandleJson().execute(url)

        btnUpdateDataPayroll.setOnClickListener {
            val jmlahTotal = tmpTotal
            var jumTotal = Integer.parseInt(jmlahTotal);

           //Perhitungan Uang Lembur Regular
            val regular = edRelugar.text.toString()
            var tmp_regular = Integer.parseInt(regular);
            val TotalLemburRegular = tmp_regular.toDouble()*12500
            val tmpTotalLemburRegular = java.lang.Double.toString(TotalLemburRegular).replace(".0", "")
            edUpdateLemburReguler.setText(tmpTotalLemburRegular)

            //Perhitungan Uang Lembur Hari Libur
            val libur = edLibur.text.toString()
            var tmp_libur = Integer.parseInt(libur);
            val TotalLemburLibur = tmp_libur.toDouble()*16500
            val tmpTotalLemburLibur = java.lang.Double.toString(TotalLemburLibur).replace(".0", "")
            edUpdateLemburLibur.setText(tmpTotalLemburLibur);

            //Perhitungan Denda Terlambat
            val terlambat = edTerlambat.text.toString()
            var tmp_terlambat = Integer.parseInt(terlambat);
            val TotalDendaTerlambat = tmp_terlambat.toDouble()*10000
            val tmpTotalDendaTerlambat = java.lang.Double.toString(TotalDendaTerlambat).replace(".0", "")
            edUpdateTerlambat.setText(tmpTotalDendaTerlambat);

            //update allowance
            val lainLain: String = edUK.getText().toString()
            val updateUangMakan: String = edUangMakan.getText().toString()
            val updateUangKerajinan: String = edUangKerajinan.getText().toString()
            val updateLemburRegular: String = edUpdateLemburReguler.getText().toString()
            val updateLemburLibur: String = edUpdateLemburLibur.getText().toString()
            val UpdateAllowance = lainLain.toDouble() + updateUangMakan.toDouble() + updateUangKerajinan.toDouble() + updateLemburRegular.toDouble() + updateLemburLibur.toDouble()
            //update deduction
            val updateTerlambat: String = edUpdateTerlambat.getText().toString()
            val updatePinjaman: String = edUpdatePinjaman.getText().toString()
            val updateIuranBPJS: String = edUpdateIuran.getText().toString()
            val UpdateDeduction = updateTerlambat.toDouble() + updatePinjaman.toDouble() + updateIuranBPJS.toDouble()

            //update Total Gaji
            var UpdateTotal = (jumTotal + UpdateAllowance)-UpdateDeduction
            tmpUpdateData = java.lang.Double.toString(UpdateTotal).replace(".0", "")

            //format Rupiah
            val formatLainLain = formatRupiah(lainLain.toDouble())
            val formatUangMakan = formatRupiah(updateUangMakan.toDouble())
            val formatUangKerajinan = formatRupiah(updateUangKerajinan.toDouble())
            val formatLemburReguler = formatRupiah(updateLemburRegular.toDouble())
            val formatupdateLemburLibur = formatRupiah(updateLemburLibur.toDouble())
            val formatupdateTerlambat = formatRupiah(updateTerlambat.toDouble())
            val formatupdatePinjaman = formatRupiah(updatePinjaman.toDouble())
            val formatupdateIuran = formatRupiah(updateIuranBPJS.toDouble())

            //custom toast
            val customLayout = layoutInflater.inflate(R.layout.activity_custom_toast_payroll, constraintlayout)


            //Show Dialog
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.alert_dialog_update_payroll, null)
            val mBuilder = AlertDialog.Builder(this).setView(mDialogView)

            mDialogView.tvBulanPayroll.setText(blnPayroll)
            mDialogView.tvNamaPayroll.setText(split[0].trim())
            mDialogView.tvUKPayroll.setText(formatLainLain)
            mDialogView.tvUangMakan.setText(formatUangMakan)
            mDialogView.tvUangKerajinan.setText(formatUangKerajinan)
            mDialogView.tvUangRegulerUpdatePayroll.setText(formatLemburReguler)
            mDialogView.tvUangLemburLiburUpdatePayroll.setText(formatupdateLemburLibur)
            mDialogView.tvTerlambatUpdatePayroll.setText(formatupdateTerlambat)
            mDialogView.tvPinjamanUpdatePayroll.setText(formatupdatePinjaman)
            mDialogView.tvIuranBPJS.setText(formatupdateIuran)

            val mAlertDialog = mBuilder.show()

            mDialogView.btnConfirmUpdatePayroll.setOnClickListener {
                val toast = Toast(this)
                toast.duration = Toast.LENGTH_SHORT
                toast.setGravity(Gravity.TOP, 0, 0)
                toast.view = customLayout
                toast.show()
                insertUpdateDataPayroll().execute()
                mAlertDialog.dismiss()
                finish()
            }
            mDialogView.btnCancelUpdatePayroll.setOnClickListener {
                mAlertDialog.dismiss()
            }

        }

    }

    //Data Payroll
    inner class AsyncTaskHandleJson : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {

            var text:String
            val connection = URL(url[0]).openConnection() as HttpURLConnection
            try {
                connection.connect()
                text = connection.inputStream.use{it.reader().use{ reader->reader.readText()}}
            }finally {
                connection.disconnect();
            }
            return text
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            handleJson(result)
        }
    }
    private fun handleJson(jsonString: String?){
        val jsonArray=  JSONArray(jsonString)
        var x = 0

        while(x<jsonArray.length()){
            val jsonObject = jsonArray.getJSONObject(x)
            tmpTotal = jsonObject.getString("gaji_pokok")
            blnPayroll = jsonObject.getString("bulan_payroll")


            tmpStringLemburReguler = jsonObject.getString("uang_lembur_regular")
            tmpStringLibur = jsonObject.getString("uang_lembur_hari_libur")
            tmpStringTerlambat = jsonObject.getString("terlambat")


            tmpIntLemburReguler = Integer.parseInt(tmpStringLemburReguler);
            tmpDoubleLemburReguler = (tmpIntLemburReguler/12500).toDouble()
            tmpLemburReguler = java.lang.Double.toString(tmpDoubleLemburReguler).replace(".0", "")
            edRegular.setText(tmpLemburReguler)

            tmpIntLemburLibur = Integer.parseInt(tmpStringLibur);
            tmpDoubleLemburLibur = (tmpIntLemburLibur/16500).toDouble()
            tmpLibur = java.lang.Double.toString(tmpDoubleLemburLibur).replace(".0", "")
            edLibur.setText(tmpLibur)

            tmpIntTerlambat = Integer.parseInt(tmpStringTerlambat);
            tmpDoubleTerlambat = (tmpIntTerlambat/10000).toDouble()
            tmpTerlambat = java.lang.Double.toString(tmpDoubleTerlambat).replace(".0", "")
            edTerlambat.setText(tmpTerlambat)




            edUK.setText(jsonObject.getString("lain_lain_UK"))
            edUangMakan.setText(jsonObject.getString("uang_makan"))
            edUpdatePinjaman.setText(jsonObject.getString("pinjaman"))
            edUpdateIuran.setText(jsonObject.getString("iuran_bpjs"))
            edUangKerajinan.setText(jsonObject.getString("uang_kerajinan"))
            x++
        }
    }

    inner class insertUpdateDataPayroll: AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {
            val requestQueue = Volley.newRequestQueue(this@UpdateDataPayroll)
            val stringRequest: StringRequest = object : StringRequest(
                Method.POST,
                getString(R.string.url).toString() + "User_HRD/update_data_payroll",
                Response.Listener { response ->
                    if (response.equals("[]", ignoreCase = true)) {
                    } else {
                        // Process response
                        try {
                            val jsonArray = JSONArray(response)
                            val jsonObject = jsonArray.getJSONObject(0)

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("VOLLEY", error.toString())
                })
            {
                override fun getParams(): Map<String, String>? {
                    val params: MutableMap<String, String> = HashMap()
                    params["idPayroll"] =  tmpIdPayroll
                    params["uangMakan"] = edUangMakan.text.toString()
                    params["uangKerajinan"] = edUangKerajinan.text.toString()
                    params["uangLemburRegular"] = edUpdateLemburReguler.text.toString()
                    params["uangLemburLibur"] = edUpdateLemburLibur.text.toString()
                    params["lain"] = edUK.text.toString()
                    params["terlambat"] = edUpdateTerlambat.text.toString()
                    params["pinjaman"] = edUpdatePinjaman.text.toString()
                    params["iuranBPJS"] = edUpdateIuran.text.toString()
                    params["total"] = tmpUpdateData
                    return params
                }
            }
            requestQueue.add(stringRequest)
            return ""
        }
    }


    //format rupiah
    private fun formatRupiah(number: Double): String? {
        val localeID = Locale("in", "ID")
        val formatRupiah = NumberFormat.getCurrencyInstance(localeID)
        return formatRupiah.format(number)
    }
}
