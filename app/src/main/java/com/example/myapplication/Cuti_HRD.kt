package com.example.myapplication

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_custom_toast.*
import kotlinx.android.synthetic.main.activity_cuti__h_r_d.*
import kotlinx.android.synthetic.main.alert_dialog.view.*
import kotlinx.android.synthetic.main.alert_dialog_cuti.view.*
import org.json.JSONArray
import org.json.JSONException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class Cuti_HRD : AppCompatActivity() {
    lateinit var session: Session
    lateinit var sessionCalendar: SessionCalendar
    lateinit var edStartDate: EditText
    lateinit var edEndDate: EditText
    lateinit var btnTambahTanggal: Button
    lateinit var btnAjukanTglCutiHRD: Button
    lateinit var arrayAdapter: ArrayAdapter<String>
    var list: ArrayList<String> = ArrayList()
    val calendar = Calendar.getInstance()
    val myFormatStart = "yyyy/MM/dd"
    val myFormatEnd = "yyyy/MM/dd"
    var listposition = 0
    var tmpDuration = ""
    @RequiresApi(Build.VERSION_CODES.O)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuti__h_r_d)
        setTitle("Pengajuan Tanggal Cuti")

        session = Session(applicationContext)
        sessionCalendar = SessionCalendar(applicationContext)

        //custom toast
        val customLayout = layoutInflater.inflate(R.layout.activity_custom_toast_submit, constraintlayout)

        //adapter
        arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, list)

        //Initialize
        edStartDate = findViewById(R.id.edStartDate)
        edEndDate = findViewById(R.id.edEndDate)
        btnTambahTanggal = findViewById(R.id.btnTambahTanggal)
        btnAjukanTglCutiHRD = findViewById(R.id.btnAjukanTglCutiHRD)


        //Session
        session.getUserDetails().get("userName")
        session.checkLogin()
        var user:HashMap<String, String> = session.getUserDetails()
        user.get(Session.KEY_NAME)!!
        user.get(Session.KEY_POSITION)!!


        //Calendar
        val dateStart =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth -> // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateLabelStart()
            }
        val dateEnd =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth -> // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateLabelEnd()
            }

        edStartDate.setOnClickListener {
            DatePickerDialog(
                this@Cuti_HRD, dateStart, calendar.get(Calendar.YEAR), calendar.get(
                    Calendar.MONTH
                ), calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }
        edEndDate.setOnClickListener {
            DatePickerDialog(
                this@Cuti_HRD, dateEnd, calendar.get(Calendar.YEAR), calendar.get(
                    Calendar.MONTH
                ), calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

       /*if(sessionCalendar.isDataExist()){
            val jsonArray = JSONArray(sessionCalendar.getDateDetails())
            list.clear()
            for (i in 0 until jsonArray.length()) {
                val jsonObject: String = jsonArray.getString(i)
                list.add(jsonObject)
            }
            listViewHRD.adapter = arrayAdapter
            arrayAdapter.notifyDataSetChanged()
        }*/


        listViewHRD.setOnItemClickListener { parent, view, position, id ->
            listposition = position

            //Show Dialog
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.alert_dialog, null)

            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setTitle("Hapus Tanggal Cuti")
            val mAlertDialog = mBuilder.show()

            mDialogView.dialogSubmitBtn.setOnClickListener {
                val posisi = listposition
                list.removeAt(posisi)
                listViewHRD.adapter = arrayAdapter
                arrayAdapter.notifyDataSetChanged()
                val jsonCalendar = Gson().toJson(list)
                sessionCalendar.saveCalendar(jsonCalendar)
                mAlertDialog.dismiss()
            }
            mDialogView.dialogCancelBtn.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }

        //Pengajuan Tanggal Cuti
        btnAjukanTglCutiHRD.setOnClickListener {
            if(list.size <=0){
                Toast.makeText(
                    this@Cuti_HRD,
                    "Jadwal cuti masih kosong",
                    Toast.LENGTH_LONG
                ).show()
            }else{
                //Show Dialog
                val mDialogView = LayoutInflater.from(this).inflate(R.layout.alert_dialog_cuti, null)
                val mBuilder = AlertDialog.Builder(this)
                    .setView(mDialogView)
                    .setTitle("Pengajuan Tanggal Cuti")
                val mAlertDialog = mBuilder.show()

                mDialogView.btnSetujuAjukan.setOnClickListener {
                    val toast = Toast(this)
                    toast.duration = Toast.LENGTH_SHORT
                    toast.setGravity(Gravity.TOP, 0, 0)
                    toast.view = customLayout
                    toast.show()
                    insertTanggalCuti().execute()
                    mAlertDialog.dismiss()
                    list.clear()
                    arrayAdapter.notifyDataSetChanged()
                }
                mDialogView.btnBatalAjukan.setOnClickListener {
                    mAlertDialog.dismiss()
                }
            }
        }


        btnTambahTanggal.setOnClickListener {
            if(edStartDate.text.toString() != "" && edEndDate.text.toString() != "") {
                listViewHRD.adapter = arrayAdapter
                arrayAdapter.notifyDataSetChanged()

                lateinit var string1: String
                lateinit var string2: String
                string1 = edStartDate.text.toString().replace("/", "-")
                string2 = edEndDate.text.toString().replace("/", "-")
                var date1 = LocalDate.parse(string1)
                var date2 = LocalDate.parse(string2)
                val dates1: Date
                val dates2: Date
                val dfd = SimpleDateFormat("yyyy-MM-dd")
                dates1 = dfd.parse(date1.toString())
                dates2 = dfd.parse(date2.toString())
                var difference: Long = (dates2.time - dates1.time)
                var differenceDates = difference / (24 * 60 * 60 * 1000)
                val dayDifference = java.lang.Long.toString(differenceDates)

                if(differenceDates<0){
                    Toast.makeText(
                        this@Cuti_HRD,
                        "Pastikan tanggal ambil cuti lebih kecil dari tanggal masuk",
                        Toast.LENGTH_LONG
                    ).show()
                }else if(dayDifference.equals("0")){
                    Toast.makeText(
                        this@Cuti_HRD,
                        "Tanggal pengambilan cuti sama dengan tanggal masuk",
                        Toast.LENGTH_LONG
                    ).show()
                }else{

                    var found: Boolean? = null
                    for (i in list.indices) {
                        val selectedIndex = list[i]
                        if(selectedIndex.contains(edStartDate.text.toString())&&selectedIndex.contains(edEndDate.text.toString())){
                            found = true
                            break
                        }
                    }

                    if (list.size <= 0){
                        var difference: Long = (dates2.time - dates1.time)
                        var differenceDates = difference / (24 * 60 * 60 * 1000)
                        var tmp = differenceDates+1
                        val dayDifference = java.lang.Long.toString(tmp)

                        list.add(edStartDate.text.toString() + " - " + edEndDate.text.toString())
                        val jsonCalendar = Gson().toJson(list)
                        sessionCalendar.saveCalendar(jsonCalendar)

                    }else {
                        if (found == true){
                        }else {
                            var difference: Long = (dates2.time - dates1.time)
                            var differenceDates = difference / (24 * 60 * 60 * 1000)
                            var tmp = differenceDates+1
                            val dayDifference = java.lang.Long.toString(tmp)
                            tmpDuration = dayDifference
                            list.add(edStartDate.text.toString() + " - " + edEndDate.text.toString())
                            val jsonCalendar = Gson().toJson(list)
                            sessionCalendar.saveCalendar(jsonCalendar)
                        }
                    }
                }
            }else{
                Toast.makeText(
                    this@Cuti_HRD,
                    "Tanggal pengambilan cuti dan tanggal masuk kosong",
                    Toast.LENGTH_LONG
                ).show()
            }
            edStartDate.setText("")
            edEndDate.setText("")
        }
    }


    private fun updateLabelStart() {
        val sdf = SimpleDateFormat(myFormatStart, Locale.US)
        edStartDate.setText(sdf.format(calendar.getTime()))
    }
    private fun updateLabelEnd() {
        val sdf = SimpleDateFormat(myFormatEnd, Locale.US)
        edEndDate.setText(sdf.format(calendar.getTime()))
    }


    inner class insertTanggalCuti : AsyncTask<String, String, String>(){
        override fun doInBackground(vararg url: String?): String {
            val requestQueue = Volley.newRequestQueue(this@Cuti_HRD)
            val stringRequest: StringRequest = object : StringRequest(
                Method.POST,
                getString(R.string.url).toString() + "User_HRD/insert_tanggal_cuti",
                Response.Listener { response ->
                    if (response.equals("[]", ignoreCase = true)) {
                    } else {
                        // Process response
                        try {
                            val jsonArray = JSONArray(response)
                            val jsonObject = jsonArray.getJSONObject(0)

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                    }
                },
                Response.ErrorListener { error ->
                    Log.e("VOLLEY", error.toString())
                })
            {
                override fun getParams(): Map<String, String>? {
                    val params: MutableMap<String, String> = HashMap()
                    var tanggal = String()
                    if (sessionCalendar.getDateDetails() != null) {
                        tanggal = sessionCalendar.getDateDetails()
                    }
                    params["id"] = session.getUserDetails().get("userID").toString()
                    params["tanggal"] = tanggal
                    return params
                }
            }
            requestQueue.add(stringRequest)
            return ""
        }
    }
}
